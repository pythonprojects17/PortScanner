import socket
import threading
from sys import argv, exit
from queue import Queue
from art import tprint


EXPECTED_ARGUMENT = 3
ports = Queue()

if len(argv) != EXPECTED_ARGUMENT:
    exit("portScanner.py <target ip> <Port range>")


def fill_queue(ports_range):
    """
    function fill the queue with all the port in the range.

    :return: None
    """
    global ports

    start, end = ports_range.split('-')

    for i in range(int(start), int(end) + 1):
        ports.put(i)


def port_possible(target, port):
    """
    function try to connect to the target with some port.

    :param target: a target ip
    :type target: str
    :param port: a random port
    :type port: int
    :return: if the port is open
    :rtype: bool
    """
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(2)
        sock.connect((target, port))
        sock.close()
        return True

    except socket.error:
        return False


def attack(target):
    """
    function scan the target

    :param target: a target to attack.
    :type target: str
    :return: None
    :rtype: None
    """
    global ports

    while not ports.empty():
        port = ports.get()
        if port_possible(target, port):
            print('[*] port: {}'.format(port))


def main():
    print('Welcome to')
    tprint('Port   Scanner !')
    print('\n\nOpen ports:')

    fill_queue(argv[2])
    threads = []

    for _ in range(300):
        threads.append(threading.Thread(target=attack, args=(argv[1], )))

    for t in threads:
        t.start()

    for t in threads:
        t.join()


if __name__ == '__main__':
    main()
